package main

import (
	"log"

	nats "github.com/nats-io/nats.go"
	types "gitlab.com/detlax/sugar"
)

func testUserWrite(nc *nats.Conn) {
	payload := make(types.Dictionary)
	payload["__WEBVIEW_SERVICE_currentUserId"] = "ag0p5_AJ7EU2ZM6Z"
	payload["__WEBVIEW_SERVICE_action"] = "createUser"
	payload["login"] = "popuk"
	payload["name"] = "Sasha"
	payload["surname"] = "Cherny"

	log.Println("Debug> testNats.go> testUserWrite returned with = ", GQLProcessRequest(nc, payload))
}

func testUserRead(nc *nats.Conn) {
	payload := make(types.Dictionary)
	payload["__WEBVIEW_SERVICE_currentUserId"] = "1o20KKkW6WBWsogq"
	payload["__WEBVIEW_SERVICE_action"] = "getUser"
	payload["id"] = "5Ye0Jud0kfMgbLiF"

	log.Println("Debug> testNats.go> testUserRead returned with = ", GQLProcessRequest(nc, payload))
}

func testUserDelete(nc *nats.Conn) {
	payload := make(types.Dictionary)
	payload["__WEBVIEW_SERVICE_currentUserId"] = "1o20KKkW6WBWsogq"
	payload["__WEBVIEW_SERVICE_action"] = "deleteUser"
	payload["userID"] = "CtbKWzVmByZiKosq"

	log.Println("Debug> testNats.go> testUserDelete returned with = ", GQLProcessRequest(nc, payload))
}

func testDocumentWrite(nc *nats.Conn) {
	payload := make(types.Dictionary)
	payload["__WEBVIEW_SERVICE_currentUserId"] = "1o20KKkW6WBWsogq"
	payload["__WEBVIEW_SERVICE_action"] = "createDocument"
	payload["name"] = "Финансовый отчёт за 2020 год ПАО МТС"
	payload["content"] = "Утверждено на собрании совета директоров ПАО МТС Россия чётвертого квартала 2020 года"

	log.Println("Debug> testNats.go> testDocumentWrite returned with = ", GQLProcessRequest(nc, payload))
}

func testDocumentRead(nc *nats.Conn) {
	payload := make(types.Dictionary)
	payload["__WEBVIEW_SERVICE_currentUserId"] = "1o20KKkW6WBWsogq"
	payload["__WEBVIEW_SERVICE_action"] = "getDevice"
	payload["id"] = "9fX2OMhPY3iEVHzK"

	log.Println("Debug> testNats.go> testDocumentRead returned with = ", GQLProcessRequest(nc, payload))
}

func testDocumentsRead(nc *nats.Conn) {
	payload := make(types.Dictionary)
	payload["__WEBVIEW_SERVICE_currentUserId"] = "1o20KKkW6WBWsogq"
	payload["__WEBVIEW_SERVICE_action"] = "getDevices"

	log.Println("Debug> testNats.go> testDocumentsRead returned with = ", GQLProcessRequest(nc, payload))
}

func main() {
	nc, err := nats.Connect("127.0.0.1:4222")
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		log.Fatal(err)
	}
	defer ec.Close()

	InitRequestsList()
	testUserWrite(nc)
}
