package main

import (
	"log"

	nats "github.com/nats-io/nats.go"
	types "gitlab.com/detlax/sugar"
	nattools "gitlab.com/detlax/nattools"
)

var RequestsList map[string]GQLRequest
func InitRequestsList () {
	RequestsList =  map[string]GQLRequest {
		"createDocument": 	new(GQLCommandDocumentCreate), // name, content
		"updateDocument": 	new(GQLCommandDocumentUpdate), // id name content
		"deleteDocument": 	new(GQLCommandDocumentDelete), // id
		"createUser": 		new(GQLCommandUserCreate),     // name surname login
		"updateUser": 		new(GQLCommandUserUpdate),     // id name surname login
		"deleteUser": 		new(GQLCommandUserDelete),     // id

		"getDevices": 		new(GQLQueryDevicesTable),     //
		"getDevice": 		new(GQLQueryDeviceById),       // id
		"getUsers": 		new(GQLQueryUsersTable),  
		"getUser": 			new(GQLQueryUserById),         // id
		"getUserPreview": 	new(GQLQueryUserPreviewById),  // id
		"loginExists": new(GQLQueryLoginExists),      	   // login
	}
}

type GQLQueryDevicesTable struct {}
func (g *GQLQueryDevicesTable) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.Document.Query.GetAllDocumentsPreview"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLQueryDeviceById struct {}
func (g *GQLQueryDeviceById) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.Document.Query.GetById"
	response = nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)

	// Merging it with data of user names for user that created and last modified

	testingAction = "eim.User.Query.GetById"
	userRequest := make(types.Dictionary)
	userRequest["id"] = response["agentLM"].(string)

	userResponse := nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, userRequest)
	response["agentLMName"] = userResponse["name"].(string) + " " + userResponse["surname"].(string)

	testingAction = "eim.User.Query.GetById"
	userRequest = make(types.Dictionary)
	userRequest["id"] = response["agentCR"].(string)

	userResponse = nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, userRequest)
	response["agentCRName"] = userResponse["name"].(string) + " " + userResponse["surname"].(string)

	return response
}

type GQLQueryLoginExists struct {}
func (g *GQLQueryLoginExists) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.User.Query.CheckLoginExists"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLQueryUsersTable struct {}
func (g *GQLQueryUsersTable) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.User.Query.GetAllNames"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLQueryUserById struct {}
func (g *GQLQueryUserById) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.User.Query.GetById"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLQueryUserPreviewById struct {}
func (g *GQLQueryUserPreviewById) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.User.Query.GetNameById"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLCommandDocumentCreate struct {}
func (g *GQLCommandDocumentCreate) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.Document.Command.Create"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLCommandDocumentUpdate struct {}
func (g *GQLCommandDocumentUpdate) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.Document.Command.Update"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLCommandDocumentDelete struct {}
func (g *GQLCommandDocumentDelete) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.Document.Command.Delete"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLCommandUserCreate struct {}
func (g *GQLCommandUserCreate) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.User.Command.Create"
	response = nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
	return response
}

type GQLCommandUserUpdate struct {}
func (g *GQLCommandUserUpdate) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.User.Command.Update"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLCommandUserDelete struct {}
func (g *GQLCommandUserDelete) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	testingAction := "eim.User.Command.Delete"
	return nattools.Transmit(nc, request["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, request)
}

type GQLRequest interface {
	Execute(nc *nats.Conn, request types.Dictionary) (response types.Dictionary)
}

type EmptyRequest struct {}
func (g *EmptyRequest) Execute (nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	log.Println("Debug> GQLRequests.go> ERROR: EmptyRequest is executed. This is NOT normal.")
	return request
}

func generateRequest (action string) GQLRequest {
	if req := RequestsList[action]; req != nil {
		return req
	}
	return new(EmptyRequest)
}

func GQLProcessRequest(nc *nats.Conn, request types.Dictionary) (response types.Dictionary) {
	response = (generateRequest(request["__WEBVIEW_SERVICE_action"].(string))).Execute(nc, request)
	return response
}
