--
--	CONFIDENTIAL | INTERNAL SOURCE CODE
--	© Abstract by Detlax 2020
--

box.cfg{listen = 3301}

box.schema.space.create('KVServiceData')
KVServiceData = box.space.KVServiceData

KVServiceData:format({
        {name = 'key', type = 'string'},
        {name = 'value', type = 'string'}
})

KVServiceData:create_index('primary', {
        type = 'tree',
        parts = {'key'}
})


------------------------------------------------
----             eventStream                ----
------------------------------------------------

box.schema.space.create('eventStream')
eventStream = box.space.eventStream

eventStream:format({
        {name = 'eventId', type = 'string'},
        {name = 'data', type = 'map'}
})

eventStream:create_index('primary', {
    type = 'tree',
    parts = {'eventId'}
})

------------------------------------------------
----                Users                   ----
------------------------------------------------

box.schema.space.create('Users')
Users = box.space.Users

Users:format({
        {name = 'userId', type = 'string'},
        {name = 'login', type = 'string'},
        {name = 'name', type = 'string'},
        {name = 'surname', type = 'string'},
        {name = 'agentLM', type = 'string'},
        {name = 'dateLM', type = 'string'},
        {name = 'agentCR', type = 'string'},
        {name = 'dateCR', type = 'string'},
})

Users:create_index('primary', {
        type = 'tree',
        parts = {'userId'}
})

Users:create_index('loginlookup', {
        type = 'tree',
        parts = {'login'}
})

------------------------------------------------
-----             UsersIDName             ------
------------------------------------------------
-- Unlike Users table, this on is permanent, so when user will be deleted, the entry
-- wont, instead it will be renamed to name+(DELETED)
------------------------------------------------

box.schema.space.create('UsersIDName')
UsersIDName = box.space.UsersIDName

UsersIDName:format({
        {name = 'userId', type = 'string'},
        {name = 'fullname', type = 'string'}
})

UsersIDName:create_index('primary', {
        type = 'tree',
        parts = {'userId'}
})

------------------------------------------------
-----              Documents              ------
------------------------------------------------

box.schema.space.create('Documents')
Documents = box.space.Documents

Documents:format({
        {name = 'documentId', type = 'string'},
        {name = 'name', type = 'string'},
        {name = 'content', type = 'string'},
        {name = 'agentLM', type = 'string'},
        {name = 'dateLM', type = 'string'},
        {name = 'agentCR', type = 'string'},
        {name = 'dateCR', type = 'string'},
})


Documents:create_index('primary', {
        type = 'tree',
        parts = {'documentId'}
})

------------------------------------------------
-----         Documents preview           ------
------------------------------------------------

box.schema.space.create('DocumentPreview')
DocumentPreview = box.space.DocumentPreview

DocumentPreview:format({
        {name = 'documentId', type = 'string'},
        {name = 'name', type = 'string'},
        {name = 'contentpreview', type = 'string'},
        {name = 'dateLM', type = 'string'},
})

DocumentPreview:create_index('primary', {
        type = 'tree',
        parts = {'documentId'}
})


------------------------------------------------
--
--               Delete everyting!
--
------------------------------------------------

function clearAll () 
        eventStream:truncate{}
        Users:truncate{}
        UsersIDName:truncate{}
        Documents:truncate{}
        DocumentPreview:truncate{}
        KVServiceData:truncate{}
end

function clearTables () 
        Users:truncate{}
        UsersIDName:truncate{}
        Documents:truncate{}
        DocumentPreview:truncate{}
end

function recreateFromLog () 
        Users:truncate{}
        UsersIDName:truncate{}
        Documents:truncate{}
        DocumentPreview:truncate{}
        KVServiceData:truncate{}
end
