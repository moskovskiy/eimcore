box.cfg{listen = 3301}

UserServiceData = box.schema.space.create('UserServiceData')

UserServiceData:format({
        {name = 'key', type = 'string'},
        {name = 'value', type = 'string'}
})

UserServiceData:create_index('primary', {
        type = 'tree',
        parts = {'key'}
})


------------------------------------------------
----             eventStream                ----
------------------------------------------------

UserEventStream = box.schema.space.create('UserEventStream')

UserEventStream:format({
        {name = 'eventId', type = 'string'},
        {name = 'data', type = 'map'}
})

UserEventStream:create_index('primary', {
    type = 'tree',
    parts = {'eventId'}
})


------------------------------------------------
----                Users                   ----
------------------------------------------------

Users = box.schema.space.create('Users')

Users:format({
        {name = 'userId', type = 'string'},
        {name = 'login', type = 'string'},
        {name = 'name', type = 'string'},
        {name = 'surname', type = 'string'},
        {name = 'agentLM', type = 'string'},
        {name = 'dateLM', type = 'string'},
        {name = 'agentCR', type = 'string'},
        {name = 'dateCR', type = 'string'},
})

Users:create_index('primary', {
        type = 'tree',
        parts = {'userId'}
})

Users:create_index('loginlookup', {
        type = 'tree',
        parts = {'login'}
})

------------------------------------------------
-----             UsersIDName             ------
------------------------------------------------
-- Unlike Users table, this on is permanent, so when user will be deleted, the entry
-- wont, instead it will be renamed to name+(DELETED)
------------------------------------------------

UsersIDName = box.schema.space.create('UsersIDName')

UsersIDName:format({
        {name = 'userId', type = 'string'},
        {name = 'fullname', type = 'string'}
})

UsersIDName:create_index('primary', {
        type = 'tree',
        parts = {'userId'}
})

------------------------------------------------
--
--               Delete everyting!
--
------------------------------------------------

function UsersClearAll () 
        UsersEventStream:truncate{}
        UsersServiceData:truncate{}
        Users:truncate{}
        UserPreview:truncate{}
end

function UsersClearTables () 
        Users:truncate{}
        UserPreview:truncate{}
end

function UsersRecreateFromLog () 
        Users:truncate{}
        UserPreview:truncate{}
        UsersEventStream:truncate{}
end
