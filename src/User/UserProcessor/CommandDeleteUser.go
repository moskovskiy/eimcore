package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
)

func (e *deleteUser) handle() {
	Users := new(tools.TarantoolSpace)
	Users.LoadSpace(e.connection, "Users")

	UsersIDName := new(tools.TarantoolSpace)
	UsersIDName.LoadSpace(e.connection, "UsersIDName")

	oldName := ""
	obj := UsersIDName.GetElementByID(e.userID, "primary")
	for _, element := range obj {
		parsed := element.([]interface{})
		oldName = parsed[1].(string)
	}

	oldName = "DELETED (" + oldName + ")"

	Users.Delete(e.userID, "primary")
	UsersIDName.Delete(e.userID, "primary")

	//UsersIDName.Update(e.userID, "primary", "fullname", "=", oldName)

	log.Println("Debug> UserCommandhandle.go> Deleting user : {\n     userID: ", e.userID)
}

/* -----------

   Delete User

------------ */

type deleteUser struct {
	connection *tools.Tarantool

	userID string
}

func (e *deleteUser) commandName() string {
	return "eim.User.Command.Delete"
}

func (e *deleteUser) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["userID"]; ok && (data["userID"] != "<nil>") {
		e.userID, ok = v.(string)
		if !ok {
			e.userID = ""
		}
	} else {
		e.userID = ""
	}
}
