package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
	"strconv"
)

func (e *updateUser) handle() {

	Users := new(tools.TarantoolSpace)
	Users.LoadSpace(e.connection, "Users")

	UsersIDName := new(tools.TarantoolSpace)
	UsersIDName.LoadSpace(e.connection, "UsersIDName")

	if (e.name != "") && (e.surname != "") {
		Users.Update(e.userID, "primary", "name", "=", e.name)
		Users.Update(e.userID, "primary", "surname", "=", e.surname)
		UsersIDName.Update(e.userID, "primary", "fullname", "=", e.name+" "+e.surname)
	}

	Users.Update(e.userID, "primary", "dateLM", "=", tools.GenerateStringDateFromUnix(e.time))
	Users.Update(e.userID, "primary", "agentLM", "=", e.agentID)

	if (e.login != "") && (!thisLoginIsTaken(e.login, e.connection)) {
		Users.Update(e.userID, "primary", "login", "=", e.login)
	}

	log.Println("Debug> UserCommandhandle.go> Modifying user : {\n     userID: ", e.userID, "\n     login: ", e.login, "\n     name: ", e.name, "\n     surname: ", e.surname, "\n     agentID: ", e.agentID, "\n     agentname: ", "?", "\n     date: ", tools.GenerateStringDateFromUnix(e.time), "\n}")
}

/* -----------

   Modify User

------------ */

type updateUser struct {
	connection *tools.Tarantool

	userID  string
	login   string
	name    string
	surname string

	agentID string
	time    int
}

func (e *updateUser) commandName() string {
	return "eim.User.Command.Update"
}

func (e *updateUser) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["__ABSTRACT_SERVICE_agentID"]; ok && (data["__ABSTRACT_SERVICE_agentID"] != "<nil>") {
		e.agentID, ok = v.(string)
		if !ok {
			e.agentID = "7PFFL2_UCvdMO8Av"
		}
	} else {
		e.agentID = "7PFFL2_UCvdMO8Av"
	}

	timestr := ""
	if v, ok := data["time"]; ok && (data["time"] != "<nil>") {
		timestr, ok = v.(string)
		if !ok {
			timestr = ""
		}
	} else {
		timestr = ""
	}

	time := 0
	time, err := strconv.Atoi(timestr)
	if err != nil {
		e.time = tools.GetUnixTime()
		log.Println("Debug> UserCommand.go> Time conversion from string to int failed")
	} else {
		e.time = time
	}

	if v, ok := data["login"]; ok && (data["login"] != "<nil>") {
		e.login, ok = v.(string)
		if !ok {
			e.login = ""
		}
	} else {
		e.login = ""
	}

	if v, ok := data["name"]; ok && (data["name"] != "<nil>") {
		e.name, ok = v.(string)
		if !ok {
			e.name = ""
		}
	} else {
		e.name = ""
	}

	if v, ok := data["surname"]; ok && (data["surname"] != "<nil>") {
		e.surname, ok = v.(string)
		if !ok {
			e.surname = ""
		}
	} else {
		e.surname = ""
	}

	if v, ok := data["userID"]; ok && (data["userID"] != "<nil>") {
		e.userID, ok = v.(string)
		if !ok {
			e.userID = ""
		}
	} else {
		e.userID = ""
	}
}
