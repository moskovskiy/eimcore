package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
	"strconv"
)

func (e *createUser) handle() {
	Users := new(tools.TarantoolSpace)
	Users.LoadSpace(e.connection, "Users")

	UsersIDName := new(tools.TarantoolSpace)
	UsersIDName.LoadSpace(e.connection, "UsersIDName")

	serial := new(tools.RandomID)
	serial.Initialize(tools.IDGenerationAlphabetDefault)

	generatedID := serial.GenerateTupleID("")

	login := ""
	if (e.login != "") && (!thisLoginIsTaken(e.login, e.connection)) {
		login = e.login
	} else {
		login = generatedID
	}

	a := Users.Add(types.Tuple{
		generatedID,
		login,
		e.name,
		e.surname,
		e.agentID,
		tools.GenerateStringDateFromUnix(e.time), // e.event.date (?)
		e.agentID,
		tools.GenerateStringDateFromUnix(e.time),
	})

	b := UsersIDName.Add(types.Tuple{
		generatedID,
		(e.name + " " + e.surname),
	})

	log.Println("Debug> UserCommandhandle.go> Added = ", a && b, " user ", generatedID, ": {\n     login: ", e.login, "\n     name: ", e.name, "\n     surname: ", e.surname, "\n     agentID: ", e.agentID, "\n     agentname: ", "?", "\n     date: ", tools.GenerateStringDateFromUnix(e.time), "\n}")
}

/* -----------

   Add User

------------ */

type createUser struct {
	connection *tools.Tarantool

	login   string
	name    string
	surname string
	agentID string
	time    int
}

func (e *createUser) commandName() string {
	return "eim.User.Command.Create"
}

func (e *createUser) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["__ABSTRACT_SERVICE_agentID"]; ok && (data["__ABSTRACT_SERVICE_agentID"] != "<nil>") {
		e.agentID, ok = v.(string)
		if !ok {
			e.agentID = "7PFFL2_UCvdMO8Av"
		}
	} else {
		e.agentID = "7PFFL2_UCvdMO8Av"
	}

	timestr := ""
	if v, ok := data["time"]; ok && (data["time"] != "<nil>") {
		timestr, ok = v.(string)
		if !ok {
			timestr = ""
		}
	} else {
		timestr = ""
	}

	time := 0
	time, err := strconv.Atoi(timestr)
	if err != nil {
		e.time = tools.GetUnixTime()
		log.Println("Debug> UserCommand.go> Time conversion from string to int failed")
	} else {
		e.time = time
	}

	if v, ok := data["login"]; ok && (data["login"] != "<nil>") {
		e.login, ok = v.(string)
		if !ok {
			e.login = ""
		}
	} else {
		e.login = ""
	}

	if v, ok := data["name"]; ok && (data["name"] != "<nil>") {
		e.name, ok = v.(string)
		if !ok {
			e.name = ""
		}
	} else {
		e.name = ""
	}

	if v, ok := data["surname"]; ok && (data["surname"] != "<nil>") {
		e.surname, ok = v.(string)
		if !ok {
			e.surname = ""
		}
	} else {
		e.surname = ""
	}
}
