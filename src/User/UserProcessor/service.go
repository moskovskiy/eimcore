package main

import (
	tools "gitlab.com/detlax/tarantools"
)

const previewSize = 35

func isCorrectLogin(s string) bool {
	for _, r := range s {
		if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') && (r < '0' || r > '9') && (r != '_') {
			return false
		}
	}
	return true
}

func thisLoginIsTaken(login string, connection *tools.Tarantool) bool {

	// checks if it is correct e.g A-Za-z0-9_

	Users := new(tools.TarantoolSpace)
	Users.LoadSpace(connection, "Users")

	obj := Users.GetElementByID(login, "loginlookup")

	if obj != nil {
		for range obj {
			return true && isCorrectLogin(login)
		}
	}

	return false
}
