package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (g *getAllUsernames) request() {
	UsersIDName := new(tools.TarantoolSpace)
	UsersIDName.LoadSpace(g.connection, "UsersIDName")

	obj := UsersIDName.GetAll("primary")

	size := 0
	for range obj {
		size++
	}

	g.names = make([]types.Dictionary, size)

	for index, element := range obj {
		parsed := element.(types.Tuple)
		(g.names[index]) = make(types.Dictionary)
		(g.names[index])["id"] = parsed[0]
		(g.names[index])["name"] = parsed[1]
	}
}

/* -----------

   Get all users names - for preview

------------ */

type getAllUsernames struct {
	connection *tools.Tarantool
	id         string

	names []types.Dictionary
}

func (g *getAllUsernames) queryName() string {
	return "eim.User.Query.GetAllNames"
}

func (g *getAllUsernames) linkDB(t *tools.Tarantool) {
	g.connection = t
}

func (g *getAllUsernames) unmarshalRequest(request types.Dictionary) {}

func (g *getAllUsernames) marshalResponse() (result types.Dictionary) {
	result = make(types.Dictionary)
	result["users"] = g.names
	return result
}
