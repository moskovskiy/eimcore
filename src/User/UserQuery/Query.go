package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
)

// Do not forget to register your query
func initQueriesList() {
	queryList = map[string]Query{
		"eim.User.Query.GetById":          new(getUserByID),
		"eim.User.Query.GetNameById":      new(getUsernameByID),
		"eim.User.Query.GetAllNames":      new(getAllUsernames),
		"eim.User.Query.CheckLoginExists": new(checkLoginExists),
	}
}

// Query query interface
type Query interface {
	queryName() string
	linkDB(t *tools.Tarantool)
	unmarshalRequest(request types.Dictionary)
	request()
	marshalResponse() (result types.Dictionary)
}

var queryList map[string]Query

// GenerateQueryFromName returns query object from string name
func GenerateQueryFromName(name string) Query {
	initQueriesList()
	query := queryList[name]
	if query != nil {
		return query
	}

	return new(emptyQuery)
}

// ExecuteQuery executes query object
func ExecuteQuery(q Query, request types.Dictionary) (result types.Dictionary) {
	q.unmarshalRequest(request)
	q.request()
	return q.marshalResponse()
}

//
//	Default object
//

type emptyQuery struct{}

func (q *emptyQuery) queryName() string {
	log.Println("Debug>WARNING! EmptyQuery is called instead of the real one. This is NOT normal")
	return ""
}

func (q *emptyQuery) linkDB(t *tools.Tarantool) {
	log.Println("Debug>WARNING! EmptyQuery is called instead of the real one. This is NOT normal")
}

func (q *emptyQuery) unmarshalRequest(request types.Dictionary) {
	log.Println("Debug>WARNING! EmptyQuery is called instead of the real one. This is NOT normal")
}

func (q *emptyQuery) request() {
	log.Println("Debug>WARNING! EmptyQuery is called instead of the real one. This is NOT normal")
}

func (q *emptyQuery) marshalResponse() (result types.Dictionary) {
	log.Println("Debug>WARNING! EmptyQuery is called instead of the real one. This is NOT normal")
	return make(types.Dictionary)
}
