--
--	CONFIDENTIAL | INTERNAL SOURCE CODE
--	© Abstract by Detlax 2020
--

box.cfg{listen = 3301}

box.schema.space.create('DocumentServiceData')
DocumentServiceData = box.space.DocumentServiceData

DocumentServiceData:format({
        {name = 'key', type = 'string'},
        {name = 'value', type = 'string'}
})

DocumentServiceData:create_index('primary', {
        type = 'tree',
        parts = {'key'}
})


------------------------------------------------
----             eventStream                ----
------------------------------------------------

box.schema.space.create('DocumentEventStream')
DocumentEventStream = box.space.DocumentEventStream

DocumentEventStream:format({
        {name = 'eventId', type = 'string'},
        {name = 'data', type = 'map'}
})

DocumentEventStream:create_index('primary', {
    type = 'tree',
    parts = {'eventId'}
})

------------------------------------------------
-----              Documents              ------
------------------------------------------------

box.schema.space.create('Documents')
Documents = box.space.Documents

Documents:format({
        {name = 'documentId', type = 'string'},
        {name = 'name', type = 'string'},
        {name = 'content', type = 'string'},
        {name = 'agentLM', type = 'string'},
        {name = 'dateLM', type = 'string'},
        {name = 'agentCR', type = 'string'},
        {name = 'dateCR', type = 'string'},
})


Documents:create_index('primary', {
        type = 'tree',
        parts = {'documentId'}
})

------------------------------------------------
-----         Documents preview           ------
------------------------------------------------

box.schema.space.create('DocumentPreview')
DocumentPreview = box.space.DocumentPreview

DocumentPreview:format({
        {name = 'documentId', type = 'string'},
        {name = 'name', type = 'string'},
        {name = 'contentpreview', type = 'string'},
        {name = 'dateLM', type = 'string'},
})

DocumentPreview:create_index('primary', {
        type = 'tree',
        parts = {'documentId'}
})


------------------------------------------------
--
--               Delete everyting!
--
------------------------------------------------

function DocumentsClearAll () 
        DocumentEventStream:truncate{}
        DocumentServiceData:truncate{}
        Documents:truncate{}
        DocumentPreview:truncate{}
end

function DocumentsClearTables () 
        Documents:truncate{}
        DocumentPreview:truncate{}
end

function DocumentsRecreateFromLog () 
        Documents:truncate{}
        DocumentPreview:truncate{}
        DocumentEventStream:truncate{}
end
