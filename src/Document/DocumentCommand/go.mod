module DocumentCommand

go 1.12

require (
	github.com/nats-io/nats.go v1.10.0
	github.com/tarantool/go-tarantool v0.0.0-20191229181800-f4ece3508d87
	gitlab.com/detlax/sugar v0.0.0-20200720101204-8abcbc567db1
	gitlab.com/detlax/tarantools v0.0.0-20200720084617-3374bdfd6412
)
