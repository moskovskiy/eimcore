package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func writeEvent(tarantoolDB *tools.Tarantool, data types.Dictionary) (id string) {
	eventStream := new(tools.TarantoolSpace)
	eventStream.LoadSpace(tarantoolDB, "DocumentEventStream")

	serial := new(tools.RandomID)
	serial.Initialize(tools.IDGenerationAlphabetDefault)

	time := tools.GetUnixTime()
	serialString := tools.GenerateTimeBytes(time, tools.IDGenerationAlphabetDefault) + serial.GenerateTupleID("")

	data["time"] = time

	eventStream.Add(types.Tuple{
		serialString,
		data,
	})

	// Update last processed element. Warning! Order of elements in eventStream select is not always alphabetical for id

	KVServiceData := new(tools.TarantoolSpace)
	KVServiceData.LoadSpace(tarantoolDB, "DocumentServiceData")
	KVServiceData.SetValueKV("lastInsertedEvent", serialString)

	return serialString
}
