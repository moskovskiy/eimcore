package main

import (
	"encoding/json"
	"log"
	"strings"
	"sync"
	"time"

	nats "github.com/nats-io/nats.go"
	"github.com/tarantool/go-tarantool"
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

type JSONdictionary = string

func toJSON(in types.Dictionary) (out JSONdictionary) {
	jsonString, err := json.Marshal(in)
	if err != nil {
		log.Fatal(err)
	}

	out = string(jsonString[:len(jsonString)])
	return out
}

func fromJSON(in JSONdictionary) (out types.Dictionary) {
	out = make(types.Dictionary)
	return out
}

var tarantoolDB *tools.Tarantool

func main() {
	tarantoolDB = new(tools.Tarantool)
	tarantoolDB.Connect("127.0.0.1:3600", tarantool.Opts{
		Timeout:       3600 * time.Second,
		Reconnect:     1 * time.Second,
		MaxReconnects: 3600,
		User:          "guest",
		Pass:          "",
	})

	nc, err := nats.Connect("127.0.0.1:4222")
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		log.Fatal(err)
	}
	defer ec.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)
	log.Println("Debug> NATSCommandListener.go> Starting processing commands")

	_, err = ec.Subscribe("eim.Document.Command.>", func(req *nats.Msg) {
		payloadRaw := req.Data
		subj := req.Subject

		payloadString := strings.ReplaceAll(string(payloadRaw[:len(payloadRaw)]), "\\", "")
		payloadByte := []byte(payloadString)

		payload := make(types.Dictionary)
		log.Println("Debug> NATSCommandListener.go> Trying to unmarshal = ", payloadString)
		err = json.Unmarshal(payloadByte, &payload)

		if err != nil {
			log.Println(err)
		} else {
			log.Println("Debug> NATSCommandListener.go> Subscribe to ", subj, " recieved payload = ", payload)
			writeEvent(tarantoolDB, payload)
			log.Println("Debug> NATSCommandListener.go> WriteEvent completed")
			nc.Publish(req.Reply, make([]byte, 1))
		}
	})

	if err != nil {
		log.Println("Debug> NATSRequestListener.go> Subscribe failed with error = ", err)
	}

	wg.Wait()
}
