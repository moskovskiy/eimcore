package main

import (
	"log"
	"strconv"

	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (e *createDocument) handle() {
	Documents := new(tools.TarantoolSpace)
	Documents.LoadSpace(e.connection, "Documents")

	DocumentPreview := new(tools.TarantoolSpace)
	DocumentPreview.LoadSpace(e.connection, "DocumentPreview")

	serial := new(tools.RandomID)
	serial.Initialize(tools.IDGenerationAlphabetDefault)

	generatedID := serial.GenerateTupleID("")

	// Filling up query tables

	Documents.Add(types.Tuple{
		generatedID,
		e.name,
		e.content,
		e.agentID,
		tools.GenerateStringDateFromUnix(e.time),
		e.agentID,
		tools.GenerateStringDateFromUnix(e.time),
	})

	generatedContent := ""
	if len(e.content) < previewSize {
		generatedContent = e.content
	} else {
		generatedContent = e.content[:previewSize] + "..."
	}

	DocumentPreview.Add(types.Tuple{
		generatedID,
		e.name,
		generatedContent,
		tools.GenerateStringDateFromUnix(e.time),
	})

	log.Println("Debug> CommandCreateDocument.go> Adding document ", generatedID, ": {\n      name: ", e.name, "\n     content: ", e.content, "\n     agentID: ", e.agentID, "\n     agentname: ", "?", "\n     date: ", tools.GenerateStringDateFromUnix(e.time), "\n}")
}

/* -----------

   Creates Document

------------ */

type createDocument struct {
	connection *tools.Tarantool

	name    string
	content string
	agentID string
	time    int
}

func (e *createDocument) commandName() string {
	return "eim.Document.Command.Create"
}

func (e *createDocument) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["__ABSTRACT_SERVICE_agentID"]; ok && (data["__ABSTRACT_SERVICE_agentID"] != "<nil>") {
		e.agentID, ok = v.(string)
		if !ok {
			e.agentID = "7PFFL2_UCvdMO8Av"
		}
	} else {
		e.agentID = "7PFFL2_UCvdMO8Av"
	}

	timestr := ""
	if v, ok := data["time"]; ok && (data["time"] != "<nil>") {
		timestr, ok = v.(string)
		if !ok {
			timestr = ""
		}
	} else {
		timestr = ""
	}

	time := 0
	time, err := strconv.Atoi(timestr)
	if err != nil {
		e.time = tools.GetUnixTime()
		log.Println("Debug> CommandCreateDocument.go> Time conversion from string to int failed")
	} else {
		e.time = time
	}

	if v, ok := data["content"]; ok && (data["content"] != "<nil>") {
		e.content, ok = v.(string)
		if !ok {
			e.content = ""
		}
	} else {
		e.content = ""
	}

	if v, ok := data["name"]; ok && (data["name"] != "<nil>") {
		e.name, ok = v.(string)
		if !ok {
			e.name = ""
		}
	} else {
		e.name = ""
	}
}
