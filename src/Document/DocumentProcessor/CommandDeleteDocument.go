package main

import (
	"log"

	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (e *deleteDocument) handle() {
	Documents := new(tools.TarantoolSpace)
	Documents.LoadSpace(e.connection, "Documents")

	DocumentPreview := new(tools.TarantoolSpace)
	DocumentPreview.LoadSpace(e.connection, "DocumentPreview")

	Documents.Delete(e.documentID, "primary")
	DocumentPreview.Delete(e.documentID, "primary")

	log.Println("Debug> DocumentCommandHandle.go> Deleting document : {\n     dId: ", e.documentID)
}

/* -----------

   Delete Document

------------ */

type deleteDocument struct {
	connection *tools.Tarantool

	documentID string
}

func (e *deleteDocument) commandName() string {
	return "eim.Document.Command.Delete"
}

func (e *deleteDocument) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["documentID"]; ok && (data["documentID"] != "<nil>") {
		e.documentID, ok = v.(string)
		if !ok {
			e.documentID = ""
		}
	} else {
		e.documentID = ""
	}
}
