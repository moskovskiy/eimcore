package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
)

/* ================================

Explaination: how to make new commands that will be called when added to DocumnentEventStream
in order to make query table

1. Register struct for it: e.g.

type MyNewCoolCommand {
	event Event //technical info will be stored here

	//store any data you need here, e.g
	variable1 string
	variable2 int
}

2. Implement Register - it's identical for everyone - just save event technical info

func (command *MyNewCoolCommand) Register (data Event) {
	command.event = data
}

3. Implement CommandName - just name teller

func (command *MyNewCoolCommand) CommandName () string {
	return "myNewCoolCommand"
}

4. Implement Unmarshal - data will be sent as a map[string]interface{} e.g. Dictionary type payload - save it
to struct internal data to process later.

! DO NOT FORGET ! to check if this key exists and it has a correct type: see example below

func (command *MyNewCoolCommand) Unmarshal (data Dictionary) {
	if v, ok := data ["variable1"]; ok && (data ["variable1"] != "<nil>") {
		e.variable1, ok = v.(string)
		if !ok {
			e.variable1 = "defaultValue"
		}
	} else {
		e.variable1 = "defaultValue"
	}
}

Yikes! Better be just `data ["variable1"].(string)` but type cast checks are important, so app won't crash you know!

5. Implement Handle - here you have to make all of the changes to query tables using data you saved during unmarshal

func (command *MyNewCoolCommand) Handle () {
	//... Do something here with query tables according to the command
}

6. Register your command in initCommandsList below
=============================== */

// Processable command interface
type Processable interface {
	commandName() string
	unmarshal(data types.Dictionary, conn *tools.Tarantool)
	handle()
}

// Execute executes processable command when it arrives
func executeCommand(command Processable, payload types.Dictionary, eventID string, connection *tools.Tarantool) {
	command.unmarshal(payload, connection)
	command.handle()
	updateLastProcessedEvent(eventID, connection)
}

var commandList map[string]Processable

func initCommandsList() {
	commandList = map[string]Processable{
		"eim.Document.Command.Create": new(createDocument),
		"eim.Document.Command.Update": new(updateDocument),
		"eim.Document.Command.Delete": new(deleteDocument),
	}
}

func generateCommandFromName(name string) Processable {
	if command := commandList[name]; command != nil {
		return command
	}
	return new(emptyCommand)
}

func processCommandsList(connection *tools.Tarantool, commands []interface{}) {
	initCommandsList()

	log.Println("Debug> Command.go> Got commands: ", commands)

	for _, element := range commands {

		payload := tools.ParseMap(element.([]interface{})[1].(map[interface{}]interface{}))

		action := ""
		if v, ok := payload["__ABSTRACT_SERVICE_action"]; ok {
			action, ok = v.(string)
			if !ok {
				action = ""
			}
		} else {
			action = ""
		}

		command := generateCommandFromName(action)

		log.Println("Debug> Command.go> Executing command with payload: ", payload)

		eventID := ""
		if v, ok := element.([]interface{}); ok {
			eventID, ok = v[0].(string)
			if !ok {
				eventID = ""
			}
		} else {
			eventID = ""
		}

		executeCommand(command, payload, eventID, connection)
	}
}

func updateLastProcessedEvent(id string, connection *tools.Tarantool) {
	KVServiceData := new(tools.TarantoolSpace)
	KVServiceData.LoadSpace(connection, "DocumentServiceData")
	KVServiceData.SetValueKV("lastProcessedEvent", id)

	// If for any reason lastInsertedEvent is missing, initialize it with the same data to avoid looping
	// in stream reader

	/*kvLastInsertedRaw := KVServiceData.GetElementByID("lastInsertedEvent", "primary")

	kvLastInsertedRawSize := 0
	for range kvLastInsertedRaw {
		kvLastInsertedRawSize++
	}

	if kvLastInsertedRawSize <= 0 {
		KVServiceData.SetValueKV("lastInsertedEvent", id)
		return
	}*/
}

// ------------
//
// This is default command - it does nothing
//
// ------------

type emptyCommand struct{}

func (d *emptyCommand) commandName() string {
	log.Println("Debug>WARNING! EmptyCommand is called instead of the real one. This is NOT normal")
	return "emptyCommand"
}

func (d *emptyCommand) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	log.Println("Debug>WARNING! EmptyCommand is called instead of the real one. This is NOT normal")
}

func (d *emptyCommand) handle() {
	log.Println("Debug>WARNING! EmptyCommand is called instead of the real one. This is NOT normal")
}
