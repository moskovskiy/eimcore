package main

import (
	"log"
	"strconv"

	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

const previewSize = 36

func (e *updateDocument) handle() {

	Documents := new(tools.TarantoolSpace)
	Documents.LoadSpace(e.connection, "Documents")

	DocumentPreview := new(tools.TarantoolSpace)
	DocumentPreview.LoadSpace(e.connection, "DocumentPreview")

	if e.name != "" {
		Documents.Update(e.documentID, "primary", "name", "=", e.name)
		DocumentPreview.Update(e.documentID, "primary", "name", "=", e.name)
	}

	generatedContent := ""
	if len(e.content) < previewSize {
		generatedContent = e.content
	} else {
		generatedContent = e.content[:previewSize] + "..."
	}

	if e.content != "" {
		Documents.Update(e.documentID, "primary", "content", "=", e.content)
		DocumentPreview.Update(e.documentID, "primary", "contentpreview", "=", generatedContent)
	}

	Documents.Update(e.documentID, "primary", "dateLM", "=", tools.GenerateStringDateFromUnix(e.time))
	DocumentPreview.Update(e.documentID, "primary", "dateLM", "=", tools.GenerateStringDateFromUnix(e.time))
	Documents.Update(e.documentID, "primary", "agentLM", "=", e.agentID)

	log.Println("Debug> CommandUpdateDocument.go> Updating document ", e.documentID, ": {\n      name: ", e.name, "\n     surname: ", e.content, "\n     agentID: ", e.agentID, "\n     agentname: ", "?", "\n     date: ", tools.GenerateStringDateFromUnix(e.time), "\n}")
}

/* -----------

   Update Document

------------ */

type updateDocument struct {
	connection *tools.Tarantool

	documentID string
	name       string
	content    string
	agentID    string
	time       int
}

func (e *updateDocument) commandName() string {
	return "eim.Document.Command.Update"
}

func (e *updateDocument) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["__ABSTRACT_SERVICE_agentID"]; ok && (data["__ABSTRACT_SERVICE_agentID"] != "<nil>") {
		e.agentID, ok = v.(string)
		if !ok {
			e.agentID = "7PFFL2_UCvdMO8Av"
		}
	} else {
		e.agentID = "7PFFL2_UCvdMO8Av"
	}

	timestr := ""
	if v, ok := data["time"]; ok && (data["time"] != "<nil>") {
		timestr, ok = v.(string)
		if !ok {
			timestr = ""
		}
	} else {
		timestr = ""
	}

	time := 0
	time, err := strconv.Atoi(timestr)
	if err != nil {
		e.time = tools.GetUnixTime()
		log.Println("Debug> UserCommand.go> Time conversion from string to int failed")
	} else {
		e.time = time
	}

	if v, ok := data["content"]; ok && (data["content"] != "<nil>") {
		e.content, ok = v.(string)
		if !ok {
			e.content = ""
		}
	} else {
		e.content = ""
	}

	if v, ok := data["name"]; ok && (data["name"] != "<nil>") {
		e.name, ok = v.(string)
		if !ok {
			e.name = ""
		}
	} else {
		e.name = ""
	}

	if v, ok := data["documentID"]; ok && (data["documentID"] != "<nil>") {
		e.documentID, ok = v.(string)
		if !ok {
			e.documentID = ""
		}
	} else {
		e.documentID = ""
	}
}
