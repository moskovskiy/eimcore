package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
	"time"

	"github.com/tarantool/go-tarantool"
)

func checkNewEventDiff(connection *tools.Tarantool) (bool, string) {
	KVServiceData := new(tools.TarantoolSpace)
	KVServiceData.LoadSpace(connection, "DocumentServiceData")

	log.Println(connection)
	log.Println(KVServiceData)
	kvLastInsertedRaw := KVServiceData.GetElementByID("lastInsertedEvent", "primary")
	kvLastProcessedRaw := KVServiceData.GetElementByID("lastProcessedEvent", "primary")

	kvLastInsertedRawSize := 0
	for range kvLastInsertedRaw {
		kvLastInsertedRawSize++
	}

	kvLastProcessedRawSize := 0
	for range kvLastProcessedRaw {
		kvLastProcessedRawSize++
	}

	if (kvLastProcessedRawSize == 0) || (kvLastInsertedRawSize == 0) {
		return true, ""
	}

	kvLastInserted := kvLastInsertedRaw[0].([]interface{})[1].(string)
	kvLastProcessed := kvLastProcessedRaw[0].([]interface{})[1].(string)

	return (kvLastInserted > kvLastProcessed), kvLastProcessed
}

func main() {
	tarantoolDB := new(tools.Tarantool)

	print(tarantoolDB)

	tarantoolDB.Connect("127.0.0.1:3600", tarantool.Opts{
		Timeout:       3600 * time.Second,
		Reconnect:     1 * time.Second,
		MaxReconnects: 3600,
		User:          "guest",
		Pass:          "",
	})

	stream := new(tools.TarantoolSpace)
	stream.LoadSpace(tarantoolDB, "DocumentEventStream")
	noCommandsPrinted := false

	for {
		time.Sleep(10 * time.Nanosecond)
		// Select when to re-check for new events added to update query table
		// This means that larger the gap - more frequently the read table wont be synced right away!

		newCommands, lastProcessedID := checkNewEventDiff(tarantoolDB)

		if newCommands {
			noCommandsPrinted = false
			if lastProcessedID != "" {
				log.Println("Debug> StreamRead.go> Recieved new events: starting processing after ", lastProcessedID)
			}
			commands := stream.Get(0, 100, tarantool.IterGt, "primary", types.Tuple{lastProcessedID})
			processCommandsList(tarantoolDB, commands)
		} else {
			if !noCommandsPrinted {
				log.Println("Debug> StreamRead.go> Recieving no new events")
				noCommandsPrinted = true
			}
		}

	}
}
