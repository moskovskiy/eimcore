package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (g *getDocumentPreviewByID) request() {
	DocumentPreview := new(tools.TarantoolSpace)
	DocumentPreview.LoadSpace(g.connection, "DocumentPreview")

	obj := DocumentPreview.GetElementByID(g.id, "primary")
	for _, element := range obj {
		parsed := element.([]interface{})
		g.name = parsed[1].(string)
		g.contentpreview = parsed[2].(string)
	}
}

/* ------

   Get document preview by id - name and 20 first letters of content

------- */

type getDocumentPreviewByID struct {
	connection *tools.Tarantool
	id         string

	name           string
	contentpreview string
}

func (g *getDocumentPreviewByID) queryName() string {
	return "eim.Document.Query.GetPreviewById"
}

func (g *getDocumentPreviewByID) linkDB(t *tools.Tarantool) {
	g.connection = t
}

func (g *getDocumentPreviewByID) unmarshalRequest(request types.Dictionary) {
	ok := true

	g.id, ok = request["id"].(string)
	if !ok {
		g.id = ""
	}
}

func (g *getDocumentPreviewByID) marshalResponse() (result types.Dictionary) {
	result = make(types.Dictionary)
	result["name"] = g.name
	result["contentpreview"] = g.contentpreview
	return result
}
