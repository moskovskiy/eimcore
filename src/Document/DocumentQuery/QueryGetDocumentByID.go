package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (g *getDocumentByID) request() {
	Documents := new(tools.TarantoolSpace)
	Documents.LoadSpace(g.connection, "Documents")

	obj := Documents.GetElementByID(g.id, "primary")
	for _, element := range obj {
		parsed := element.([]interface{})
		g.name = parsed[1].(string)
		g.content = parsed[2].(string)
		g.agentLM = parsed[3].(string)
		g.dateLM = parsed[4].(string)
		g.agentCR = parsed[5].(string)
		g.dateCR = parsed[6].(string)
	}
}

/* ------

   Get full document by Id

------- */

type getDocumentByID struct {
	connection *tools.Tarantool
	id         string

	name    string
	content string
	agentLM string
	dateLM  string
	agentCR string
	dateCR  string
}

func (g *getDocumentByID) queryName() string {
	return "eim.Document.Query.getDocumentByID"
}

func (g *getDocumentByID) linkDB(t *tools.Tarantool) {
	g.connection = t
}

func (g *getDocumentByID) unmarshalRequest(request types.Dictionary) {
	ok := true

	g.id, ok = request["id"].(string)
	if !ok {
		g.id = ""
	}
}

func (g *getDocumentByID) marshalResponse() (result types.Dictionary) {
	result = make(types.Dictionary)
	result["name"] = g.name
	result["content"] = g.content
	result["agentLM"] = g.agentLM
	result["agentCR"] = g.agentCR
	result["dateLM"] = g.dateLM
	result["dateCR"] = g.dateCR
	return result
}
