package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (g *getAllDocumentsPreview) request() {
	DocumentPreview := new(tools.TarantoolSpace)
	DocumentPreview.LoadSpace(g.connection, "DocumentPreview")

	obj := DocumentPreview.GetAll("primary")

	size := 0
	for range obj {
		size++
	}

	g.previews = make([]types.Dictionary, size)

	for index, element := range obj {
		parsed := element.(types.Tuple)
		(g.previews[index]) = make(types.Dictionary)
		(g.previews[index])["id"] = parsed[0]
		(g.previews[index])["name"] = parsed[1]
		(g.previews[index])["contentpreview"] = parsed[2]
		(g.previews[index])["dateLM"] = parsed[3]
	}
}

/* ------

   Get listed table of all document previews - name, first 20 letters and date of last modification - reporting table

------- */

type getAllDocumentsPreview struct {
	connection *tools.Tarantool

	previews []types.Dictionary
}

func (g *getAllDocumentsPreview) queryName() string {
	return "eim.Document.Query.getAllDocumentsPreview"
}

func (g *getAllDocumentsPreview) linkDB(t *tools.Tarantool) {
	g.connection = t
}

func (g *getAllDocumentsPreview) unmarshalRequest(request types.Dictionary) {}

func (g *getAllDocumentsPreview) marshalResponse() (result types.Dictionary) {
	result = make(types.Dictionary)
	result["documents"] = g.previews
	return result
}
