box.cfg{listen = 3301}

WorkflowServiceData = box.schema.space.create('WorkflowServiceData')

WorkflowServiceData:format({
        {name = 'key', type = 'string'},
        {name = 'value', type = 'string'}
})

WorkflowServiceData:create_index('primary', {
        type = 'tree',
        parts = {'key'}
})


------------------------------------------------
----             eventStream                ----
------------------------------------------------

WorkflowEventStream = box.schema.space.create('WorkflowEventStream')

WorkflowEventStream:format({
        {name = 'eventId', type = 'string'},
        {name = 'data', type = 'map'}
})

WorkflowEventStream:create_index('primary', {
    type = 'tree',
    parts = {'eventId'}
})


------------------------------------------------
----                Workflows                   ----
------------------------------------------------

Workflows = box.schema.space.create('Workflows')

Workflows:format({
        {name = 'WorkflowId', type = 'string'},
        {name = 'name', type = 'string'},
        {name = 'description', type = 'string'},
        {name = 'steps', type = 'array'},
        {name = 'agentLM', type = 'string'},
        {name = 'dateLM', type = 'string'},
        {name = 'agentCR', type = 'string'},
        {name = 'dateCR', type = 'string'},
})

Workflows:create_index('primary', {
        type = 'tree',
        parts = {'WorkflowId'}
})

------------------------------------------------
-----             WorkflowsPreview       ------
------------------------------------------------
-- Unlike Workflows table, this on is permanent, so when Workflow will be deleted, the entry
-- wont, instead it will be renamed to name+(DELETED)
------------------------------------------------

WorkflowsPreview = box.schema.space.create('WorkflowsPreview')

WorkflowsPreview:format({
        {name = 'WorkflowId', type = 'string'},
        {name = 'name', type = 'string'},
        {name = 'stepspreview', type = 'string'},
        {name = 'descriptionpreview', type = 'string'},
})

WorkflowsPreview:create_index('primary', {
        type = 'tree',
        parts = {'WorkflowId'}
})

------------------------------------------------
--
--               Delete everyting!
--
------------------------------------------------

function WorkflowsClearAll () 
        WorkflowsEventStream:truncate{}
        WorkflowsServiceData:truncate{}
        Workflows:truncate{}
        WorkflowPreview:truncate{}
end

function WorkflowsClearTables () 
        Workflows:truncate{}
        WorkflowPreview:truncate{}
end

function WorkflowsRecreateFromLog () 
        Workflows:truncate{}
        WorkflowPreview:truncate{}
        WorkflowsEventStream:truncate{}
end
