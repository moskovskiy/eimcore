package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
	"strconv"
)

func stepsPreview(steps []string) string {
	return "Step1 -> Step2"
}

func (e *createWorkflow) handle() {
	Workflows := new(tools.TarantoolSpace)
	Workflows.LoadSpace(e.connection, "Workflows")

	WorkflowsPreview := new(tools.TarantoolSpace)
	WorkflowsPreview.LoadSpace(e.connection, "WorkflowsPreview")

	serial := new(tools.RandomID)
	serial.Initialize(tools.IDGenerationAlphabetDefault)

	generatedID := serial.GenerateTupleID("")

	res := Workflows.Add(types.Tuple{
		generatedID,
		e.name,
		e.description,
		e.steps,
		e.agentID,
		tools.GenerateStringDateFromUnix(e.time),
		e.agentID,
		tools.GenerateStringDateFromUnix(e.time),
	})

	log.Println(res)

	generatedContent := ""
	if len(e.description) < previewSize {
		generatedContent = e.description
	} else {
		generatedContent = e.description[:previewSize] + "..."
	}

	res = WorkflowsPreview.Add(types.Tuple{
		generatedID,
		e.name,
		stepsPreview(e.steps),
		generatedContent,
	})

}

/* -----------

   Add Workflow

------------ */

type createWorkflow struct {
	connection *tools.Tarantool

	name        string
	description string
	steps       []string
	agentID     string
	time        int
}

func (e *createWorkflow) commandName() string {
	return "eim.Workflow.Command.Create"
}

func (e *createWorkflow) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["__ABSTRACT_SERVICE_agentID"]; ok && (data["__ABSTRACT_SERVICE_agentID"] != "<nil>") {
		e.agentID, ok = v.(string)
		if !ok {
			e.agentID = "7PFFL2_UCvdMO8Av"
		}
	} else {
		e.agentID = "7PFFL2_UCvdMO8Av"
	}

	timestr := ""
	if v, ok := data["time"]; ok && (data["time"] != "<nil>") {
		timestr, ok = v.(string)
		if !ok {
			timestr = ""
		}
	} else {
		timestr = ""
	}

	time := 0
	time, err := strconv.Atoi(timestr)
	if err != nil {
		e.time = tools.GetUnixTime()
		log.Println("Debug> WorkflowCommand.go> Time conversion from string to int failed")
	} else {
		e.time = time
	}

	log.Println("Steps: ", data["steps"])
	if v, ok := data["steps"]; ok && (data["steps"] != "<nil>") {
		e.steps, ok = v.([]string)
		if !ok {
			e.steps = []string{""}
		}
	} else {
		e.steps = []string{""}
	}

	log.Println("Steps: ", data["steps"].(string))

	if v, ok := data["name"]; ok && (data["name"] != "<nil>") {
		e.name, ok = v.(string)
		if !ok {
			e.name = ""
		}
	} else {
		e.name = ""
	}

	if v, ok := data["description"]; ok && (data["description"] != "<nil>") {
		e.description, ok = v.(string)
		if !ok {
			e.description = ""
		}
	} else {
		e.description = ""
	}
}
