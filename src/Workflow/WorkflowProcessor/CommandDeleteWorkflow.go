package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
)

func (e *deleteWorkflow) handle() {
	Workflows := new(tools.TarantoolSpace)
	Workflows.LoadSpace(e.connection, "Workflows")

	WorkflowsIDName := new(tools.TarantoolSpace)
	WorkflowsIDName.LoadSpace(e.connection, "WorkflowsPreview")

	oldName := ""
	obj := WorkflowsIDName.GetElementByID(e.WorkflowID, "primary")
	for _, element := range obj {
		parsed := element.([]interface{})
		oldName = parsed[1].(string)
	}

	oldName = "DELETED (" + oldName + ")"

	Workflows.Delete(e.WorkflowID, "primary")
	WorkflowsIDName.Delete(e.WorkflowID, "primary")

	//WorkflowsIDName.Update(e.WorkflowID, "primary", "fullname", "=", oldName)

	log.Println("Debug> WorkflowCommandhandle.go> Deleting Workflow : {\n     WorkflowID: ", e.WorkflowID)
}

/* -----------

   Delete Workflow

------------ */

type deleteWorkflow struct {
	connection *tools.Tarantool

	WorkflowID string
}

func (e *deleteWorkflow) commandName() string {
	return "eim.Workflow.Command.Delete"
}

func (e *deleteWorkflow) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["WorkflowID"]; ok && (data["WorkflowID"] != "<nil>") {
		e.WorkflowID, ok = v.(string)
		if !ok {
			e.WorkflowID = ""
		}
	} else {
		e.WorkflowID = ""
	}
}
