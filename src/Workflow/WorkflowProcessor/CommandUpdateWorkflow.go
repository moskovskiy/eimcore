package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
	"strconv"
)

func (e *updateWorkflow) handle() {

	Workflows := new(tools.TarantoolSpace)
	Workflows.LoadSpace(e.connection, "Workflows")

	WorkflowsIDName := new(tools.TarantoolSpace)
	WorkflowsIDName.LoadSpace(e.connection, "WorkflowsPreview")

	if e.name != "" {
		Workflows.Update(e.WorkflowID, "primary", "name", "=", e.name)
		WorkflowsIDName.Update(e.WorkflowID, "primary", "name", "=", e.name)
	}

	Workflows.Update(e.WorkflowID, "primary", "dateLM", "=", tools.GenerateStringDateFromUnix(e.time))
	Workflows.Update(e.WorkflowID, "primary", "agentLM", "=", e.agentID)

	generatedContent := ""
	if len(e.description) < previewSize {
		generatedContent = e.description
	} else {
		generatedContent = e.description[:previewSize] + "..."
	}

	if e.description != "" {
		Workflows.Update(e.WorkflowID, "primary", "description", "=", e.description)
		WorkflowsIDName.Update(e.WorkflowID, "primary", "description", "=", generatedContent)
	}

	if len(e.steps) > 0 {
		Workflows.Update(e.WorkflowID, "primary", "steps", "=", e.steps)
	}

	log.Println("Debug> WorkflowCommandhandle.go> Modifying Workflow : {\n     WorkflowID: ", e.WorkflowID, "\n     name: ", e.name, "\n     name: ", e.name, "\n     description: ", e.description, "\n     agentID: ", e.agentID, "\n     agentname: ", "?", "\n     date: ", tools.GenerateStringDateFromUnix(e.time), "\n}")
}

/* -----------

   Modify Workflow

------------ */

type updateWorkflow struct {
	connection *tools.Tarantool

	WorkflowID  string
	name        string
	description string
	steps       []string

	agentID string
	time    int
}

func (e *updateWorkflow) commandName() string {
	return "eim.Workflow.Command.Update"
}

func (e *updateWorkflow) unmarshal(data types.Dictionary, conn *tools.Tarantool) {
	e.connection = conn

	if v, ok := data["__ABSTRACT_SERVICE_agentID"]; ok && (data["__ABSTRACT_SERVICE_agentID"] != "<nil>") {
		e.agentID, ok = v.(string)
		if !ok {
			e.agentID = "7PFFL2_UCvdMO8Av"
		}
	} else {
		e.agentID = "7PFFL2_UCvdMO8Av"
	}

	timestr := ""
	if v, ok := data["time"]; ok && (data["time"] != "<nil>") {
		timestr, ok = v.(string)
		if !ok {
			timestr = ""
		}
	} else {
		timestr = ""
	}

	time := 0
	time, err := strconv.Atoi(timestr)
	if err != nil {
		e.time = tools.GetUnixTime()
		log.Println("Debug> WorkflowCommand.go> Time conversion from string to int failed")
	} else {
		e.time = time
	}

	if v, ok := data["steps"]; ok && (data["steps"] != "<nil>") {
		e.steps, ok = v.([]string)
		if !ok {
			e.steps = []string{""}
		}
	} else {
		e.steps = []string{""}
	}

	if v, ok := data["name"]; ok && (data["name"] != "<nil>") {
		e.name, ok = v.(string)
		if !ok {
			e.name = ""
		}
	} else {
		e.name = ""
	}

	if v, ok := data["description"]; ok && (data["description"] != "<nil>") {
		e.description, ok = v.(string)
		if !ok {
			e.description = ""
		}
	} else {
		e.description = ""
	}

	if v, ok := data["WorkflowID"]; ok && (data["WorkflowID"] != "<nil>") {
		e.WorkflowID, ok = v.(string)
		if !ok {
			e.WorkflowID = ""
		}
	} else {
		e.WorkflowID = ""
	}
}
