package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
	"time"

	"github.com/tarantool/go-tarantool"
)

func stepsPreview(steps []string) string {
	return "Step1 -> Step2"
}

func main() {
	tarantoolDB := new(tools.Tarantool)

	tarantoolDB.Connect("127.0.0.1:3600", tarantool.Opts{
		Timeout:       3600 * time.Second,
		Reconnect:     1 * time.Second,
		MaxReconnects: 3600,
		User:          "guest",
		Pass:          "",
	})

	Workflows := new(tools.TarantoolSpace)
	Workflows.LoadSpace(tarantoolDB, "Workflows")

	WorkflowsPreview := new(tools.TarantoolSpace)
	WorkflowsPreview.LoadSpace(tarantoolDB, "WorkflowsPreview")

	serial := new(tools.RandomID)
	serial.Initialize(tools.IDGenerationAlphabetDefault)

	steps := []string{"See", "Buy", "Sell", "Short"}

	generatedID := serial.GenerateTupleID("")
	res := Workflows.Add(types.Tuple{
		generatedID,
		"TestName",
		"This is a test workflow",
		steps,
		"1111",
		tools.GenerateStringDateFromUnix(tools.GetUnixTime()), // e.event.date (?)
		"1111",
		tools.GenerateStringDateFromUnix(tools.GetUnixTime()),
	})

	log.Println(res)

	res = WorkflowsPreview.Add(types.Tuple{
		generatedID,
		"TestName",
		stepsPreview(steps),
		"This is a test workflow",
	})

	log.Println(res)

	obj := Workflows.GetAll("primary")

	log.Println(obj)
	size := 0
	for range obj {
		size++
	}

	arr := make([]types.Dictionary, size)
	for index, element := range obj {
		parsed := element.(types.Tuple)
		(arr[index]) = make(types.Dictionary)
		(arr[index])["id"] = parsed[0]
		(arr[index])["name"] = parsed[1]
		(arr[index])["description"] = parsed[2]
		(arr[index])["steps"] = parsed[3]
	}

	log.Println(arr[0]["steps"].([]interface{})[0].(string))
}
