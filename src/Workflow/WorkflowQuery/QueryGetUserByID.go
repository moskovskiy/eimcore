package main

/* -----------

   Get User by ID

------------ */

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (g *getUserByID) request() {
	Users := new(tools.TarantoolSpace)
	Users.LoadSpace(g.connection, "Users")

	obj := Users.GetElementByID(g.id, "primary")
	for _, element := range obj {
		parsed := element.([]interface{})
		g.login = parsed[1].(string)
		g.name = parsed[2].(string)
		g.surname = parsed[3].(string)
		g.agentLM = parsed[4].(string)
		g.dateLM = parsed[5].(string)
		g.agentCR = parsed[6].(string)
		g.dateCR = parsed[7].(string)
	}
}

type getUserByID struct {
	connection *tools.Tarantool
	id         string

	login   string
	name    string
	surname string
	agentLM string
	dateLM  string
	agentCR string
	dateCR  string
}

func (g *getUserByID) queryName() string {
	return "eim.User.Query.GetById"
}

func (g *getUserByID) linkDB(t *tools.Tarantool) {
	g.connection = t
}

func (g *getUserByID) unmarshalRequest(request types.Dictionary) {
	ok := true

	g.id, ok = request["id"].(string)
	if !ok {
		g.id = ""
	}
}

func (g *getUserByID) marshalResponse() (result types.Dictionary) {
	result = make(types.Dictionary)
	result["login"] = g.login
	result["name"] = g.name
	result["surname"] = g.surname
	result["agentLM"] = g.agentLM
	result["agentCR"] = g.agentCR
	result["dateLM"] = g.dateLM
	result["dateCR"] = g.dateCR
	return result
}
