package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (g *getUsernameByID) request() {
	UsersIDName := new(tools.TarantoolSpace)
	UsersIDName.LoadSpace(g.connection, "UsersIDName")

	obj := UsersIDName.GetElementByID(g.id, "primary")
	for _, element := range obj {
		parsed := element.([]interface{})
		g.name = parsed[1].(string)
	}
}

/* -----------

   Get User name by ID

------------ */

type getUsernameByID struct {
	connection *tools.Tarantool
	id         string

	name string
}

func (g *getUsernameByID) queryName() string {
	return "eim.User.Query.GetNameById"
}

func (g *getUsernameByID) linkDB(t *tools.Tarantool) {
	g.connection = t
}

func (g *getUsernameByID) unmarshalRequest(request types.Dictionary) {
	ok := true

	g.id, ok = request["id"].(string)
	if !ok {
		g.id = ""
	}
}

func (g *getUsernameByID) marshalResponse() (result types.Dictionary) {
	result = make(types.Dictionary)
	result["name"] = g.name
	return result
}
