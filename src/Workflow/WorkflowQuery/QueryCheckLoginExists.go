package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
)

func (g *checkLoginExists) request() {
	g.exists = false

	if g.login == "" {
		return
	}

	Users := new(tools.TarantoolSpace)
	Users.LoadSpace(g.connection, "Users")

	obj := Users.GetElementByID(g.login, "loginlookup")

	if obj != nil {
		for _, element := range obj {
			parsed := element.([]interface{})
			g.userId = parsed[0].(string)
			g.exists = true
		}
	}
}

type checkLoginExists struct {
	connection *tools.Tarantool
	login      string

	exists bool
	userId string
}

func (g *checkLoginExists) queryName() string {
	return "eim.User.Query.checkLoginExists"
}

func (g *checkLoginExists) linkDB(t *tools.Tarantool) {
	g.connection = t
}

func (g *checkLoginExists) unmarshalRequest(request types.Dictionary) {
	ok := true

	g.login, ok = request["login"].(string)
	if !ok {
		g.login = ""
	}
}

func (g *checkLoginExists) marshalResponse() (result types.Dictionary) {
	result = make(types.Dictionary)
	result["exists"] = g.exists
	result["userId"] = g.userId
	return result
}
