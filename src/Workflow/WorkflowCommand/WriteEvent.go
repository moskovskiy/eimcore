package main

import (
	types "gitlab.com/detlax/sugar"
	tools "gitlab.com/detlax/tarantools"
	"log"
)

func writeEvent(tarantoolDB *tools.Tarantool, data types.Dictionary) (id string) {
	eventStream := new(tools.TarantoolSpace)
	eventStream.LoadSpace(tarantoolDB, "WorkflowEventStream")

	serial := new(tools.RandomID)
	serial.Initialize(tools.IDGenerationAlphabetDefault)

	time := tools.GetUnixTime()
	serialString := tools.GenerateTimeBytes(time, tools.IDGenerationAlphabetDefault) + serial.GenerateTupleID("")

	data["time"] = time

	log.Println("Type here was still: ", data["steps"].([]interface{})[1])
	eventStream.Add(types.Tuple{
		serialString,
		data,
	})

	// Update last processed element. Warning! Order of elements in eventStream select is not always alphabetical for id

	KVServiceData := new(tools.TarantoolSpace)
	KVServiceData.LoadSpace(tarantoolDB, "WorkflowServiceData")
	KVServiceData.SetValueKV("lastInsertedEvent", serialString)

	return serialString
}
