export GOPATH=$GOPATH:/Users/Roma/dyne

go build ./src/Document/DocumentProcessor
go build ./src/Document/DocumentCommand
go build ./src/Document/DocumentQuery

go build ./src/User/UserProcessor
go build ./src/User/UserCommand
go build ./src/User/UserQuery
