# Microservices structure:

## Getting message from Conduct
Conduct emits commands as `_module_._domain_.Command._commandname_`
Conduct emits queries as `_module_._domain_.Query._queryname_`
e.g. `eim.Document.Query.GetById`

You will recieve payload as JSON with service fields:
__ABSTRACT_SERVICE_agentID - string ID of User - e.g. id of User domain
__ABSTRACT_SERVICE_action - string that duplicates the message name, e.g. `eim.Document.Query.GetById`

Sending data back for query - just reply to the NATS query
For command you are not obligated to reply, hence event sourcing query table making is not real time. Maybe some kind of response will be implemented in the future.

Please DO NOT read messages from other domains hence this may prevent other microservices from working correctly.

## Processing

While microservices can be made any way you want - you only should respond to NATS - there already is a strategy on how microservices are constructed:
CQRS / Event sourcing means that there are 3 microservices for each "domain"

### 1. Event stream write
-> NATS

The first microservice (usually named ***Command) recieves command and processes it. For the most part (if it is CRUD) it just means that it is written down into EventStream, like `{action:"CreateUser", id: "213123", name:"Alex"}`. See the existing microservices for more data. For more complex operations you can do anything else, the only rule is that you should NOT write anything into tables that are used for querying.

### 2. Event stream read - creating of query tables
-> Event stream

The second microservice (usually named ***Processor) reads down the event stream and produces query (read/report) tables that will be used for reading. Read tables are usually denormalized for the speed of reading.

### 3. Query responder
-> Read/Query/Report tables

The third microservice (usually named **Query) is listening for queries in NATS and responds to them with data from read tables. For most of the queries (except for direct read) it shouldn't read anything from event stream.

## Building and testing

## Packing to Docker
In order for the microservices to launch with the start of the system they have to be dockerized.