# Tarantool DB that stores and handles Abstract data


## Spinning up Abstract
1. Launch docker instance with Tarantool in it:
`docker run --publish 3600:3301 --detach --name AbstractDB tarantool/tarantool:2` -> better
`docker run --publish 3600:3301 --detach --name AbstractDB --rm -t -i -v ./setup.lua:/opt/tarantool/setup.lua tarantool/tarantool:2 /opt/tarantool/setup.lua`

2. Setup Tarantool inside instance:
`docker exec -i -t AbstractDB console`

3. Run NATS server:
`docker run -d --name nats -p 4222:4222 -p 6222:6222 -p 8222:8222 nats`

## Compiling the data 
Redirect go get gitlab.com/ to work
`git config --global url."git@gitlab.com/Detlax/abstract:".insteadOf "https://gitlab.com/Detlax/abstract"`

4. Run all of the Abstract microservices: Command, Query and Processor

## Run for correct go get 
`git config --global url."git@gitlab.com/Detlax/abstract:".insteadOf "https://gitlab.com/Detlax/abstract"`

# Abstract processor

## Idea
The idea behind Abstract is CQRS which stands for command-query responsibility seggregation. Thus all data is being sent to Abstract as a Stream, that we saw earlier. Unfortunately, stream is way to slow and complex for read operations - it will have to be played again in order to query anything. That is why Abstract has a processor inside, that creates denormalized read tables (that contain duplicate data, but are "pre-assembled" data for query).

### Event stream
Schema of user event stream is the same for all Abstract streams:
`{event_id, time_added, agent_id, action, {data}}` - agent_id is the user, that added the event/command

### Stream processor 
See `processor/Command.go` and `processor/*****Command.go`/`processor/*****CommandHandle.go` for more detail

## Query tables
See `query/Query.go` and `SpaceSetup.lua` for details